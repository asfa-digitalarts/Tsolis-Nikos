let piece = [];
let img = [];
let c = [0,1,2,3,4];
let m = 50;
let aY = [3*m,3*m,m,m,2*m];
let aX = [m,3*m,m,3*m,2*m];
let pieceIsHovered= false;
let flag = false;
let index;
//let count = [0,1,4,2,3,5,6,9,7,8];
let count = [0,1,4,2,3];
let t ;
let bird;
let birdImg;
let cord =[];
let bg = [];
let vid ;
let fsound ;
let stepsound;

function preload(){
  
  stepsound = loadSound('data/step.mp3');
  fsound = loadSound('data/for.mp3');
  vid = createVideo(['data/forrest.mp4'])
  birdImg= loadImage('data/bird0.png');
  for(let i =0; i<5;i++){
    img[i]= [];
     
    for( let j = 1; j<6; j++){
     img[i][j]= loadImage('data/'+i+'0'+j + '.png');
    }
  }
}

function setup() {
  createCanvas(650,650);
  fsound.setVolume(2);
  fsound.play();
  fsound.loop();
  vid.play();
  vid.loop();
  vid.hide();
  background(0);
  for (let i =0;i<30;i++){
  cord[i] = 150+ i*10;
  }
  //frameRate(100);
  bird = new Bird(birdImg, random(cord),random(cord));
 for(let i =0; i<5;i++){
   let r = random(c);
   piece[i]= new Piece(img[r][i+1],aX[i],aY[i],i,count[i]);
   //pieceIsHovered = false;
   //piece[i+5]= new Piece(img[random(c)][i+1],aX[i],aY[i],i+5,count[i+5]);
 }
}


function draw() {  
  background(0);
  bird.show();
  image(vid,50,50,500,500);
  pieceIsHovered = false;
  for(let i =0; i<piece.length;i++){ 
     if(piece[count[i]].isHovered() && !flag){
        pieceIsHovered = true;
        index = count[i] ;
        t= i;
        break;
    }
  }
    if(pieceIsHovered){
      cursor (HAND);
    }
    else if (flag){
      cursor(MOVE);
    }
    else{
      cursor(ARROW);
    } 
     if (flag) {
    piece[index].move(mouseX - pmouseX, mouseY - pmouseY);
  }
  for(let i =piece.length-1; i>=0;i--){
   piece[count[i]].show();
  }
  //pieceIsHovered = false;
  print("index:"+ index +"pieceIsHovered"+ pieceIsHovered);
}

function mousePressed(){
  stepsound.play();
  if(mouseButton === LEFT && pieceIsHovered){  
       flag = true; 
  }
      else{
      flag = false;      
      }     
}

function mouseReleased() {
if (mouseButton === LEFT ) {
   flag= false;
   pieceIsHovered = false;
   index= null;
  }
}
function keyPressed() {
  let temp;
  if(keyCode === UP_ARROW && t > 0){
    temp = count[t];
    count [t]=  count[t-1];
    count[t-1] = temp;
  }
  else if(keyCode === DOWN_ARROW && t < piece.length-1){
      temp = count[t];
      count [t]=  count[t+1];
      count[t+1] = temp;
  }
}
